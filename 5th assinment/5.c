#include <stdio.h>

int attendance(int ticketPrice);
int income(int ticketPrice);
int cost(int ticketPrice);
int profit(int ticketPrice);

int main ()
{
    int maxPrice=0, tPrice, attnd;
    for (int price=0; price<=45; price=price+5)
    {
        int att=attendance(price);
        int pro=profit(price);
        printf("Ticketprice:Rs.%d\tAttendance:%d\t  Profit:Rs.%d\n",price,att,pro);

        if(pro>maxPrice)
        {
            maxPrice=pro;
            tPrice=price;
            attnd=att;
        }

    }
    printf("\nMaximum profit = Rs.%d With %d Ticketprice and %d Attendence...\n\n",maxPrice,tPrice,attnd);
    return 0;

}

int attendance(int ticketPrice)
{
    return 120-(ticketPrice-15)*4;
}

int income(int ticketPrice)
{
    return ticketPrice*attendance(ticketPrice);
}

int cost(int ticketPrice)
{
    return 500+3*attendance(ticketPrice);
}

int profit(int ticketPrice)
{
    return income(ticketPrice)-cost(ticketPrice);
}


/*

int attendance(int ticketPrice);
int income(int ticketPrice);
int cost(int ticketPrice);
int profit(int ticketPrice);

int main () {
    int Price;
    printf ("Enter ticket price: ");
    scanf ("%f",&Price);
    printf ("\nProfit is Rs.%.2f\n",profit(Price));
    return 0;
}

int attendance(int ticketPrice) {
    return 120-(ticketPrice-15)*4;
}

int income(int ticketPrice) {
    return ticketPrice*attendance(ticketPrice);
}

int cost(int ticketPrice) {
    return 500+3*attendance(ticketPrice);
}

int profit(int ticketPrice) {
    return income(ticketPrice)-cost(ticketPrice);
}
*/


#include <stdio.h>
#define maxsize 50
struct student//declaration
{
    char name[maxsize];
    char subject[maxsize];
    int marks;

};
int main()
{
    int x;
    do
    {
        printf("\nHow many student's details are you going to Input?\n");
        scanf("%d",&x);
        if(x<0){
            printf("invalid input!!!\n");
        }
        else if(x<5)
        {
         printf("you Enter %d.You should input minimum 5 student's details!!!\n",x);
        }
    }
    while(x<5);
    struct student s[x];//initializing
    for(int i=0; i<x; i++) //store inputs
    {

        printf("Enter %dst student name\n",i+1);
        scanf("%s", &s[i].name);
        printf("Enter subject name\n");
        scanf("%s", &s[i].subject);
        printf("Enter marks\n");
        scanf("%d", &s[i].marks);
        printf("\n");
    }
    printf("STUDENT DETAILS:\n\n");

    for(int i=0; i<x; i++) //print details
    {
        printf("STUDENT NO-%d\n",i+1);
        printf("student name:%s\n", s[i].name);
        printf("subject:%s\n", s[i].subject);
        printf("marks:%d\n", s[i].marks);
        printf("\n");
    }
    return 0;
}

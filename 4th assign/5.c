
#include <stdio.h>

int main()
{
    int num,x,y;
    printf("Enter a any number to print table- ");
    scanf("%d", &num);

    for(x=1; x<=num; x++)
    {

        for(y=1; y<=12; y++)
        {
            printf("%d * %d = %d\n", x, y, (x*y));
        }
        printf("\n");
    }
    if(num<0)
    {
        for(x=-1; x>=num; x--)
        {

            for(y=1; y<=12; y++)
            {
                printf("%d * %d = %d\n", x, y, (x*y));
            }
            printf("\n");
        }
    }

    return 0;
}

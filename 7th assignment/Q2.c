
#include <stdio.h>
#define maxSize 100 //arrays size

int main()
{
    char str[maxSize], ch;
    int count = 0, i;

    printf("Enter a string: ");
    gets(str);

    printf("Enter a character that need the Frequency");
    scanf("%c", &ch);

    for (i = 0; str[i] != '\0'; i++)//checking whether each and every letter in the string is equal to character
    {
        if (ch == str[i])
            count++;
    }

    printf("Frequency of %c = %d", ch, count);

    return 0;
}

#include <stdio.h>
#define maxSize 50

int main()
{
    int a[maxSize][maxSize], b[maxSize][maxSize], answer[maxSize][maxSize];
    int rowA, colA, rowB, colB;
    int sum = 0, i, j, k;

    printf("Matrix Calculator \n");
    printf("...................................................................\n\n");
    printf("Enter the rows of the 1st matrix-");
    scanf("%d",&rowA);
    printf("Enter the columns of the 1st matrix-");
    scanf("%d",&colA);

    printf("Enter the elements of the 1st matrix -\n");
    for(i = 0; i < rowA; i++)
    {
        for(j = 0; j < colA; j++)// filling 1st matrix in the pattern
        {
            scanf("%d", &a[i][j]);
        }
    }
    printf("Enter the rows of the 2st matrix-");
    scanf("%d",&rowB);
    printf("Enter the columns of the 2st matrix-");
    scanf("%d",&colB);

    if(rowB != colA)//we cann't multiply if rows of 1st matrix is not equal to collm of 2nd matrix
        printf("Sorry We can not multiply these 2 matrices.");
    else
    {
        printf("Enter the elements of 2nd the matrix -\n");
        for(i = 0; i < rowB; i++)
        {
            for(j = 0; j < colB; j++)// filling 1st matrix in the pattern
            {
                scanf("%d", &b[i][j]);
            }
        }
    }
    printf("\n");//calculate the addition
    for(i = 0; i < rowA; i++)
    {
        for(j = 0; j < colB; j++)
        {
            {
                answer[i][j] = a[i][j] + b[i][j];
            }
        }
    }

    printf("\n    The addition\n");//printing addition answer
    for(i = 0; i < rowA; i++)
    {
        for(j = 0; j < colB; j++)
        {
            printf("    %d ", answer[i][j]);
        }
        printf("\n");
    }

    printf("\n");// calculate the multiplication
    for(i = 0; i < rowA; i++)
    {
        for(j = 0; j < colB; j++)
        {
            for(k = 0; k < rowB; k++)
            {
                sum = sum + (a[i][k] * b[k][j]);
            }
            answer[i][j] = sum;
            sum = 0;
        }
    }

    printf("\n    The Multiplication\n");//printing multiplication answer
    for(i = 0; i < rowA; i++)
    {
        for(j = 0; j < colB; j++)
        {
            printf("    %d ", answer[i][j]);
        }
        printf("\n");
    }


    return 0;
}



#include <stdio.h>
#include <string.h>
#define maxSize 100 //arrays size

int main()
{
    char str[maxSize];//array for store sentence
    char rev[maxSize];//array for store after the reverse the sentence
    int length, end, i;

    printf("Enter a sentence: ");
    gets(str);//function to read the string from user

    length = strlen(str);//get length with space
    end = length - 1;

    for (i = 0; i < length; i++)
    {
        rev[i] = str[end];
        end--;
    }

    rev[i] = '\0';

    puts(rev);//print the string including spacing

    return 0;
}

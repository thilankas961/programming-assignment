#include <stdio.h>

void fibonacciPattern(int x);
int fibonacciValue(int y);
int row = 0;

int main()
{
    int n;
    printf("Enter a number - ");
    scanf("%d", &n);
    fibonacciPattern(n);
    return 0;

}
//maintain the row pattern
void fibonacciPattern(int x)
{
    if (row <= x)
    {
        printf("%d\n", fibonacciValue(row));
        row++;
        fibonacciPattern(x);
    }
}
//calculate the value
int fibonacciValue(int y)
{
    if (y == 0)
    {
        return 0;
    }
    else if (y == 1)
    {
        return 1;
    }
    else
    {
        return (fibonacciValue(y - 1) + fibonacciValue(y - 2));
    }
}

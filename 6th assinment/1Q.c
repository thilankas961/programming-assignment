#include <stdio.h>

void pattern(int x);
void rowPattern(int y);
int row = 1;

int main()
{
    int n;
    printf("Enter number- ");
    scanf("%d", &n);
    pattern(n);
    return 0;
}
//print the number pattern
void rowPattern(int y)
{
    if (y > 0)
    {
        printf(" %d  ", y);
        rowPattern(y - 1);

    }
}
//maintain the pattern
void pattern(int x)
{

    if (x > 0)
    {
        rowPattern(row);
        printf("\n");
        row++;
        pattern(x - 1);
    }
}



